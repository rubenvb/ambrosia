/**
  * help_and_version_output.h
  * Helper functions that print help/usage and version information.
  *
  * Author: Ruben Van Boxem
  *
  **/

#ifndef HELP_AND_VERSION_OUTPUT_H
#define HELP_AND_VERSION_OUTPUT_H

// Global include
#include "global.h"

ambrosia_namespace_begin

void print_help_information();
void print_version_information();

ambrosia_namespace_end

#endif // HELP_AND_VERSION_OUTPUT_H
